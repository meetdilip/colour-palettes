# Colour Palettes 

I will try to keep my personal Inkscape / GIMP ( .gpl ) colour palette builds here

1. Added **Breeze Colour scheme for KDE Plasma** 

Source : https://hig.kde.org/style/color/default.html

2. Added **Breeze Dark Colour scheme for KDE Plasma** 

3. Added **Breeze Light Colour scheme for KDE Plasma** 

4. Added **Breeze High Contrast** 